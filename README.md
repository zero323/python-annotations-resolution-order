# Type annotations resolution order

Example of type resolution order with:

- PEP 561 compliant package.
  - When additional stubs are put on `MYPYPATH`.
  - When stub-only package is installed.

```sh
pip install ./foo-package
mypy main.py
MYPYPATH=$PWD/foo-stubs mypy main.py
mypy main.py
pip install ./foo-stubs
mypy main.py
```

[![asciicast](https://asciinema.org/a/454313.svg)](https://asciinema.org/a/454313)
