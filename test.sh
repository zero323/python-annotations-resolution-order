#!/usr/bin/env bash

pip install --user ./foo-package

echo "#############################"
echo "Package-only annotations"
echo "#############################"
mypy main.py
echo ""

echo "#############################"
echo "Package and stubs on MYPYPATH"
echo "#############################"
MYPYPATH=$PWD/foo-stubs mypy main.py
echo ""


echo "#############################"
echo "Back to package-only"
echo "#############################"
mypy main.py
echo ""

pip install --user ./foo-stubs

echo "#############################"
echo "Installed stubs-only-package"
echo "#############################"
mypy main.py
