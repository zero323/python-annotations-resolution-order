from setuptools import setup

setup(
    name="foo-stubs",
    version="0.0.1",
    packages=[
        "foo",
    ],
    license="GPLv3",
    install_requires=[
        "foo==0.0.1",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Typing :: Typed",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    package_data={
        "foo": [
            "py.typed",
            "__init__.pyi",
        ],
    },
)
