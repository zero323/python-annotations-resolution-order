FROM python:3.9.9-slim-bullseye

RUN useradd dev \
    && mkdir /home/dev \
    && chown dev:dev /home/dev

RUN apt-get update \
    && apt-get install -y  --no-install-recommends git pip \
    && rm -rf /var/cache/apt/*

USER dev

WORKDIR /home/dev

RUN git clone https://gitlab.com/zero323/python-annotations-resolution-order.git

WORKDIR /home/dev/python-annotations-resolution-order

ENV PATH=$PATH:/home/dev/.local/bin

RUN pip install --user --upgrade pip \
    && pip install -r requirements.txt

ENTRYPOINT ["/bin/sh"]

CMD ["test.sh"]
